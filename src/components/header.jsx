import React, { Component } from "react";

class Header extends Component {
  filterItemsBySearch = (e) => {
    let text = e.target.value.toLowerCase();
    let countriesDetails = this.props.countries;
    let filterCountry = countriesDetails.filter((country) => {
      if (country.name.official.toLowerCase().includes(text)) {
        console.log(country.name.official.toLowerCase());
        return country;
      }
    });
    this.props.inputFilter(filterCountry);
  };

  filterItemsByDropDown = (e) => {
    let text = e.target.value.toLowerCase();
    let countriesDetails = this.props.countries;
    console.log(countriesDetails);
    let filterCountry = countriesDetails.filter((country) => {
      if (country.region.toLowerCase().includes(text)) {
        console.log(country.name.official.toLowerCase());
        return country;
      }
    });
    this.props.dropDownFilter(filterCountry);
  };

  render() {
    return (
      <header className="header-content">
        <div className="title">
          <img
            src="https://www.pngplay.com/wp-content/uploads/8/World-Earth-No-Background.png"
            alt="earth-image"
            className="earth-image"
          />
          <h1>Where in the World?</h1>
        </div>
        <input
          type="text"
          className="filter"
          placeholder="Search for a Country ..."
          onChange={this.filterItemsBySearch}
        ></input>
        <select
          className="country-arrange-options"
          onChange={this.filterItemsByDropDown}
        >
          <option disabled selected>
            Filter by Region ...
          </option>
          <option className="dropdown-option">Africa</option>
          <option className="dropdown-option">Americas</option>
          <option className="dropdown-option">Asia</option>
          <option className="dropdown-option">Oceania</option>
          <option className="dropdown-option">Europe</option>
        </select>
      </header>
    );
  }
}

export default Header;
