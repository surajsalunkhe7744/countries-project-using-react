import React, { Component } from "react";

class Country extends Component {
  constructor(props) {
    super(props);
  }

  showPopUp = (event) => {
    let country = this.props.allCountries.filter((country) => {
      if (
        event.currentTarget.firstChild.nextSibling.textContent.includes(
          country.name.official
        )
      ) {
        return country;
      }
    });
    this.props.popUp(country);
  };

  render() {
    return (
      <button className="country-details" onClick={this.showPopUp}>
        <img
          src={this.props.country.flags.png}
          className="flag"
          alt="country-flag-image"
        ></img>
        <h1>{this.props.country.name.official}</h1>
        <p>
          <b>Population :</b> {this.props.country.population}
        </p>
        <p>
          <b>Region :</b> {this.props.country.region}
        </p>
        <p>
          <b>Capital :</b> {this.props.country.capital}
        </p>
      </button>
    );
  }
}

export default Country;
