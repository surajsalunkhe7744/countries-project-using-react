import React, { Component } from "react";
import Country from "./country";
import PopUp from "./popUp";
import Header from "./header";

class AllCountries extends Component {
  constructor(props) {
    super(props);

    this.state = {
      countries: [],
      filterCountries: [],
      countryPopUpData: "",
    };
  }

  componentDidMount() {
    fetch("https://restcountries.com/v3.1/all")
      .then((obj) => obj.json())
      .then((data) => {
        console.log(data);
        this.setState({
          countries: data,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  filterItemsBySearch = (filterCountry) => {
    this.setState({
      filterCountries: filterCountry,
    });
  };

  filterItemsByDropDown = (filterCountry) => {
    if (this.state.filterCountries.length === 0) {
      this.setState({
        filterCountries: this.state.countries,
      });
    } else {
      this.setState({
        filterCountries: filterCountry,
      });
    }
  };

  showPopUp = (country) => {
    if (this.state.countryPopUpData === "") {
      this.setState({
        countryPopUpData: country,
      });
    } else {
      this.setState({
        countryPopUpData: "",
      });
    }
  };

  backButton = () => {
    this.setState({
      countryPopUpData: "",
    });
  };

  render() {
    return (
      <div>
        <Header
          inputFilter={this.filterItemsBySearch}
          dropDownFilter={this.filterItemsByDropDown}
          countries={this.state.countries}
        />
        <div className="all-countries">
          {this.state.filterCountries.length !== 0
            ? this.state.filterCountries.map((country) => {
                return (
                  <Country
                    country={country}
                    popUp={this.showPopUp}
                    allCountries={this.state.filterCountries}
                    key={country.flags.png}
                  />
                );
              })
            : this.state.countries.map((country) => {
                this.setState({
                  filterCountries: this.state.countries,
                });
                return (
                  <Country
                    country={country}
                    popUp={this.showPopUp}
                    allCountries={this.state.filterCountries}
                    key={country.flags.png}
                  />
                );
              })}
        </div>
        {this.state.countryPopUpData !== "" ? (
          <PopUp
            popUpData={this.state.countryPopUpData}
            backButton={this.backButton}
          />
        ) : (
          ""
        )}
      </div>
    );
  }
}

export default AllCountries;
