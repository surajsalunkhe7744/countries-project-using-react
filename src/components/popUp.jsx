import React, { Component } from "react";

class PopUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      countryPopUpData: this.props.popUpData,
    };
  }

  removePopUp = () => {
    this.props.backButton();
  };

  render() {
    return (
      <div className="country-data-div">
        <img
          src={this.state.countryPopUpData[0].flags.png}
          alt="image"
          className="countryData-flag"
        ></img>
        <div className="country-other-details">
          <h2>{this.state.countryPopUpData[0].name.official}</h2>
          <p>
            <b>Pupulation: </b>
            {this.state.countryPopUpData[0].population
              ? this.state.countryPopUpData[0].population
              : "NA"}
          </p>
          <p>
            <b>Region: </b>
            {this.state.countryPopUpData[0].region
              ? this.state.countryPopUpData[0].region
              : "NA"}
          </p>
          <p>
            <b>Sub-Region: </b>
            {this.state.countryPopUpData[0].subregion
              ? this.state.countryPopUpData[0].subregion
              : "NA"}
          </p>
          <p>
            <b>Capital: </b>
            {this.state.countryPopUpData[0].capital}
            {this.state.countryPopUpData[0].capital
              ? this.state.countryPopUpData[0].capital
              : "NA"}
          </p>
          <p>
            <b>Top Level Domain: </b>
            {this.state.countryPopUpData[0].tld
              ? this.state.countryPopUpData[0].tld[0]
              : "NA"}
          </p>
          <p>
            <b>Current Currency: </b>
            {this.state.countryPopUpData[0].currencies
              ? Object.values(this.state.countryPopUpData[0].currencies)
                  .map((currency) => {
                    return currency.name;
                  })
                  .join("")
              : "NA"}
          </p>
          <p>
            <b>Languages: </b>
            {this.state.countryPopUpData[0].languages
              ? Object.values(this.state.countryPopUpData[0].languages)
                  .map((languages) => {
                    return languages;
                  })
                  .join(", ")
              : "NA"}
          </p>
          <p>
            <b>Borders: </b>
            {this.state.countryPopUpData[0].borders
              ? Object.values(this.state.countryPopUpData[0].borders)
                  .map((country) => {
                    return country;
                  })
                  .join(", ")
              : "NA"}
          </p>
          <button className="cancel-button" onClick={this.removePopUp}>
            Back to Countries
          </button>
        </div>
      </div>
    );
  }
}

export default PopUp;
