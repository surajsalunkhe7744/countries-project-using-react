import './App.css';
import AllCountries from './components/allCountries';

function App() {
  return (
    <div className="App">
     <AllCountries></AllCountries>
    </div>
  );
}

export default App;
